package scorpion.psikonline.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import scorpion.psikonline.model.Profession;


public interface ProfessionRepository extends JpaRepository<Profession, Integer> {

}
