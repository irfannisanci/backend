package scorpion.psikonline.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import scorpion.psikonline.model.Meeting;


public interface MeetingRepository extends JpaRepository<Meeting, Integer> {
}
