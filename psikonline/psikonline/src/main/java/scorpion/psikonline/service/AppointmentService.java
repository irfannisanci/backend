package scorpion.psikonline.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import scorpion.psikonline.model.Appointment;
import scorpion.psikonline.repository.AppointmentRepository;

import java.util.List;

@Service
public class AppointmentService {
    @Autowired
    AppointmentRepository appointmentRepository;
    public List<Appointment> getAllAppointment(){

        return appointmentRepository.findAll();
    }

    public Appointment addAppointment(Appointment appointment){
        return appointmentRepository.save(appointment);
    }

    public Appointment updateAppointment(Appointment appointment){
        Appointment newAppointment = appointmentRepository.getById(appointment.getId());
        newAppointment.setId(appointment.getId());
        newAppointment.setStartDate(appointment.getStartDate());
        newAppointment.setEndDate(appointment.getEndDate());
        newAppointment.setStatus(appointment.isStatus());
        newAppointment.setUserId(appointment.getUserId());
        return appointmentRepository.save(newAppointment);
    }

    public void deleteAppointment(int id){
        appointmentRepository.deleteById(id);
    }

    public List<Appointment> findAllAppointment(){
        return appointmentRepository.findAll();
    }
}
