package scorpion.psikonline.service.abstracts;

import java.util.List;

import scorpion.psikonline.model.PsychologistDetails;

public interface PsychologistDetailsImp {
	List<PsychologistDetails> getAllPsychologistDetails();
	PsychologistDetails createPsychologistDetails(PsychologistDetails psychologistDetails);
	PsychologistDetails updatePsychologistDetails(PsychologistDetails psychologistDetails);
	Boolean deletePsychologistDetails(int id);
}

