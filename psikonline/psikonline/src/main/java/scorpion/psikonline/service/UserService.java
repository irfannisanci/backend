package scorpion.psikonline.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import scorpion.psikonline.dtos.UserInputDto;
import scorpion.psikonline.model.User;
import scorpion.psikonline.repository.UserRepository;
import scorpion.psikonline.service.abstracts.UserImp;

@Service
public class UserService implements UserImp {

	@Autowired
	UserRepository userRepository;
	ModelMapper modelMapper;
	
	@Override
	public List<User> getAllUser(){
		return userRepository.findAll();
	}
	
	@Override
	public User createUser(UserInputDto dto) {
		User newUser=new User();
		BeanUtils.copyProperties(dto, newUser);
		return userRepository.save(newUser);
		 
	}

	@Override
	public Boolean deleteUser(int id) {
		userRepository.deleteById(id);
		return true;
	}

	@Override
	public User updateUser(User user) {
		User newUser=userRepository.getById(user.getId());
		newUser.setTckn(user.getTckn());
		newUser.setName(user.getName());
		newUser.setLastName(user.getLastName());
		newUser.setEmail(user.getEmail());
		newUser.setPassword(user.getPassword());
		return userRepository.save(newUser);
	}
	
	public List<User> getAllUserWithRole(String role){
		return userRepository.getByRole(role);
	}

	@Override
	public User findByTcknAndPassword(String tckn, String pass) {
		return userRepository.getByTcknAndPassword(tckn, pass);
	}
	
}
