package scorpion.psikonline.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import scorpion.psikonline.model.Profession;
import scorpion.psikonline.repository.ProfessionRepository;

import java.util.List;

@Service
public class ProfessionService {
    @Autowired
    ProfessionRepository professionRepository;
    public List<Profession> getAllProfession(){
        return professionRepository.findAll();

    }
    public Profession addProfession(Profession profession){

        return professionRepository.save(profession);
    }

    public Boolean deleteProfession(int id){
        professionRepository.deleteById(id);
        return true;

    }
    public Profession updateProfession(Profession profession){
        Profession newProfession=professionRepository.getById(profession.getId());
        newProfession.setId(profession.getId());
        newProfession.setPsychologistDetailId(profession.getPsychologistDetailId());
        newProfession.setProfessionName(profession.getProfessionName());

        return professionRepository.save(newProfession);


    }
}
