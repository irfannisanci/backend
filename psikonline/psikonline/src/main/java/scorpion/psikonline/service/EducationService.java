package scorpion.psikonline.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import scorpion.psikonline.model.Education;
import scorpion.psikonline.repository.EducationRepository;

import java.util.List;

@Service
public class EducationService {
    @Autowired
    EducationRepository educationRepository;
    public List<Education> getAllEducation(){
        return educationRepository.findAll();

    }
    public Education addCertificate(Education education){

        return educationRepository.save(education);
    }
    public Boolean deleteEducation(int id){
        educationRepository.deleteById(id);
        return true;

    }
    public Education UpdateEducation(Education education){

        Education newEducation =educationRepository.getById(education.getId());
        newEducation.setId(education.getId());
        newEducation.setPsychologistDetailId(education.getPsychologistDetailId());
        newEducation.setDepartment(education.getDepartment());
        newEducation.setSchoolName(education.getSchoolName());
        newEducation.setDegree(education.getDegree());
        newEducation.setStartedDate(education.getStartedDate());
        newEducation.setEndedDate(education.getEndedDate());


        return educationRepository.save(newEducation);
    }

}
