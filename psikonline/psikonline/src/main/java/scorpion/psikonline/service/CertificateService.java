package scorpion.psikonline.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import scorpion.psikonline.model.Certificate;
import scorpion.psikonline.repository.CertificateRepository;

import java.util.List;

@Service
public class CertificateService {
    @Autowired
    CertificateRepository certificateRepository;

    public List<Certificate> getAllCertificate(){
        return certificateRepository.findAll();

    }
    public Certificate addCertificate(Certificate certificate){

        return certificateRepository.save(certificate);
    }
    public Boolean deleteCertificate(int id){
        certificateRepository.deleteById(id);
        return true;

    }
    public Certificate updateCertificate(Certificate certificate){
        Certificate newCertificate=certificateRepository.getById(certificate.getId());
        newCertificate.setId(certificate.getId());
        newCertificate.setCertificateName(certificate.getCertificateName());
        newCertificate.setCertificateDate(certificate.getCertificateDate());
        newCertificate.setPsychologistDetailId(certificate.getPsychologistDetailId());
        newCertificate.setCertificateDescription(certificate.getCertificateDescription());

        return certificateRepository.save(newCertificate);

    }
}
