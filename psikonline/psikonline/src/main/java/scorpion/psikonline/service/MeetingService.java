package scorpion.psikonline.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import scorpion.psikonline.model.Meeting;
import scorpion.psikonline.repository.MeetingRepository;

import java.util.List;

@Service
public class MeetingService {
    @Autowired
    MeetingRepository meetingRepository;
    public List<Meeting> getAllMeeting(){
        return meetingRepository.findAll();
    }

    public Meeting addMeeting(Meeting meeting){
        return meetingRepository.save(meeting);
    }

    public Meeting updateMeeting(Meeting meeting){
        Meeting newMeeting = meetingRepository.getById(meeting.getId());
        newMeeting.setId(meeting.getId());
        newMeeting.setStartTime(meeting.getStartTime());
        newMeeting.setEndTime(meeting.getEndTime());
        newMeeting.setMeetingLink(meeting.getMeetingLink());
        newMeeting.setUserId(meeting.getUserId());
        return meetingRepository.save(newMeeting);
    }

    public void deleteMeeting(int id){
        meetingRepository.deleteById(id);
    }



    public List<Meeting> findAllMeeting(){
        return meetingRepository.findAll();
    }

}
