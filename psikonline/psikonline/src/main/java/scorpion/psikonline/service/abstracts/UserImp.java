package scorpion.psikonline.service.abstracts;

import java.util.List;

import scorpion.psikonline.dtos.UserInputDto;
import scorpion.psikonline.model.User;

public interface UserImp {
	List<User> getAllUser();
	User createUser(UserInputDto dto);
	User updateUser(User user);
	Boolean deleteUser(int id); 
	
	User findByTcknAndPassword(String tckn, String pass);
}
